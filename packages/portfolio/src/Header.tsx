/** @jsx jsx */
import { css, jsx } from '@emotion/core';
import { useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { ThemeContext } from './App';
import pr from './assets/pr2020.jpg';


const Header = () => {
    const theme = useContext(ThemeContext);
    const { t, i18n } = useTranslation();

    const dateStart = new Date('01-05-2012');
    const now = new Date();

    const years = Math.floor((now.getTime() - dateStart.getTime()) / (1000 * 60 * 60 * 24 * 365));

    return (
        <header
            className="App-header"
            css={css`
                display: flex;
                align-items: center;
                justify-content: space-around;
                flex-wrap: wrap;
                font-size: calc(10px + 2vmin);
                padding-top: 70px;

                background-color: ${theme.backgroundColor};

                @media only print {
                    padding-top: 0;
                }
            `}
        >
            <div
                css={css`
                    color: ${theme.textColor};

                    @media only print {
                        text-align: center;
                    }
                `}
            >
                <h1>{t('mgr inż.')} Paweł Rymsza</h1>
                <div>Senior Frontend Developer</div>
            </div>
            <img
                src={pr}
                css={css`
                    width: 55vmin;
                    height: 55vmin;
                    object-fit: cover;
                    object-position: 0 28%;
                    border-radius: 50%;
                `}
            />
            <div
                css={css`
                    font-size: 21px;
                    text-indent: 50px;

                    p {
                        margin: 0;
                    }
                `}
            >
                <p>
                    {t('Jestem absolwentem studiów magisterskich na wydziale Elektroniki Politechniki Wrocławskiej.')}{' '}
                </p>
                <p
                    css={css`
                        @media (min-width: 1011px) {
                            display: inline;
                        }
                    `}
                >
                    {t('Od ')}
                    {years}
                    {t(
                        ' lat zajmuję się wytwarzaniem oprogramowania. Pracowałem przy tworzeniu aplikacjach bankowych,'+
                        ' systemów CRM/ERP i innych typach projektów, od prostych stron internetowych aż do duże aplikacje biznesowe.'
                    )}
                {t(' Brałem udział w konsultowaniu wymagań biznesowych i tworzyłem na ich podstawie designy.')}</p>
                <p>
                    {t(
                        'Staram się kłaść duży nacisk na jakość kodu w czym pomaga mi tworzenie optymalnej ilości testów, które wprowadzam w każdej warstwie aplikacji- od metod działających na bazie danych do klikalnych testów w przeglądarce.'
                    )}
                </p>
                <p>{t('Pracowałem zarówno samodzielnie jak i w zespołach- maksymalnie kilkudziesięcioosobowych.')}</p>
            </div>
        </header>
    );
};

export { Header };

