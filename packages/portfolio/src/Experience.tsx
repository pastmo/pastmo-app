/** @jsx jsx */
import { css, jsx } from '@emotion/core';
import { useTranslation } from 'react-i18next';
import arubico from './assets/experience/arubico.jpg';
import betterit from './assets/experience/betterit.png';
import bnyMellon from './assets/experience/bny-mellon.svg';
import glasso from './assets/experience/glasso.png';
import livespace from './assets/experience/livespace.svg';
import sensortech from './assets/experience/sensortech.png';
import sygnity from './assets/experience/sygnity.svg';
import talentAlpha from './assets/experience/talent-alpha.svg';
import hsbc from './assets/experience/hsbc-logo.svg';

interface ExpType {
    date: string;
    company: string;
    logo?: string;
    mainTechnology: string;
    role: string;
    description: string;
}

const printPage = (pageData: ExpType[]) =>
    pageData.map((exp) => (
        <div
            css={css`
                display: flex;
                @media print {
                    page-break-after: avoid;
                }
            `}
            key={exp.company}
        >
            <div
                css={css`
                    flex: 0.2;
                    padding: 25px 0;
                `}
            >
                {exp.date}
            </div>
            <div
                css={css`
                    flex: 0.8;
                    display: flex;
                    border-left: 2px solid black;
                    padding: 25px;

                    @media not print {
                        flex-wrap: wrap;
                    }
                `}
            >
                <div
                    css={css`
                        width: 100px;
                        height: 50px;
                        background: url(${exp.logo ? exp.logo : ''}) no-repeat center;
                        background-size: 100% auto;
                        margin-right: 20px;
                        @media print {
                            flex: 0.3;
                        }
                    `}
                ></div>
                <div
                    css={css`
                        display: flex;
                        flex-direction: column;

                        @media print {
                            flex: 1;
                        }

                        p {
                            margin-bottom: 0;
                        }
                    `}
                >
                    <strong>{exp.company}</strong>
                    <span
                        css={css`
                            max-width: 600px;
                            font-style: italic;
                        `}
                    >
                        {exp.role}
                    </span>
                    <p
                        css={css`
                            max-width: 600px;
                        `}
                    >
                        {exp.mainTechnology}
                    </p>
                    <p
                        css={css`
                            max-width: 600px;
                        `}
                    >
                        {exp.description}
                    </p>
                </div>
            </div>
        </div>
    ));

const Experience = () => {
    const { t, i18n } = useTranslation();

    const expData: ExpType[][] = [
        [
            {
                date: '12.2021 - ',
                company: 'HSBC',
                logo: hsbc,
                role: 'Senior Frontend Developer',
                mainTechnology: 'Angular, GCP, Jenkins, Balsamiq Wireframes',
                description: t(
                    'Tworzenie monorepo w NX, migracja starych aplikacji do nowej technologii, konsultacja wymagań biznesowych i tworzenie designów, tworzenie pipeline`ów za pomocą Jenkinsa'
                ),
            },
            {
                date: '06.2020 - 11.2021',
                company: 'BNY Mellon',
                logo: bnyMellon,
                role: 'Senior Frontend Developer',
                mainTechnology: 'Angular',
                description: t(
                    'Tworzenie biblioteki dla Aplikacji angularowych, tworzenie aplikacji do wyświetlania Mikrofrondenów, testowanie aplikacji'
                ),
            },
            {
                date: '06.2019 - 04.2020',
                company: 'Talent Alpha Sp. z o.o.',
                logo: talentAlpha,
                role: 'Frontend Developer',
                mainTechnology: 'Angular, React',
                description: t(
                    'Implementacja otrzymanych designów, dbanie o jakość kodu z pomocą testów i code review'
                ),
            },
            {
                date: '08.2018 - 08.2019',
                company: 'Livespace Sp. z o.o.',
                logo: livespace,
                role: t('Programista PHP'),
                mainTechnology: 'PHP7, Zend Framework 1/3, JQuery, PostgreSQL, Redis, Vagrant, Docker',
                description: t(
                    'Implementacja nowych funkcjonalności zgodnie z wymaganiami biznesowymi, utrzymywanie istniejącego kodu. Integracja z Microsoft Graph i Google Calendar'
                ),
            },
        ],
        [
            {
                date: '05.2016 - 03.2020',
                company: t('Grupa Glasso'),
                logo: glasso,
                role: 'FullStack Developer',
                mainTechnology:
                    'Angular7, PHP5/7, Zend Framework2/3, PostgreSQL, MySQL JavaScript, Jquery, Jasmine, Bootstrap, Karma, Git, Bitbucket, Redis',
                description: t('Tworzenie dedykowanego systemu CRM/ERP dla firmy produkcyjnej'),
            },
            {
                date: '09.2015 - 02.2016',
                company: 'Arubico',
                logo: arubico,
                role: t('Programista PHP/JavaScript'),
                mainTechnology: 'JavaScript, PHP, Virtualbox',
                description: t('Tworzenie aplikacji ERP w technologii php/javascript'),
            },
            {
                date: '06.2015 - 09.2016',
                company: 'Mediatech Sp. z o.o.',
                role: t('Programista PHP/JavaScript'),
                mainTechnology: 'PHP, JavaScript, JQurey, CSS, HTML, Github, Photoshop',
                description: t('Tworzenie gry przeglądarkowej'),
            },
            {
                date: '03.2015 - 07.2015',
                company: 'BetterIT',
                logo: betterit,
                role: t('Programista PHP/JavaScript'),
                mainTechnology: 'PHP, JavaScript, JQurey, CSS, HTML',
                description: t('Tworzenie stron opartych na Wordpressie. Integracja systemów SuiteCRM, Redmine'),
            },
            {
                date: '07.2013 - 11.2014',
                company: 'Sygnity',
                logo: sygnity,
                role: 'Programista JEE',
                mainTechnology: t('JEE, PrimeFace, Weblogic, SVN'),
                description: t('Tworzenie systemu bankowości elektronicznej'),
            },
        ],
        [
            {
                date: '05.2012 - 03.2013',
                company: 'SensorTech SA',
                logo: sensortech,
                role: t('Projektant Układów Elektroniki'),
                mainTechnology: 'C, C++, Eagle',
                description: t(
                    'Tworzenie aplikacji na sterowniki cyfrowe. Tworzenie aplikacji desktopowych wspomagających działanie produkcji. Tworzenie makiet testowych'
                ),
            },
        ],
    ];

    const data1 = expData.map((page, index) => (
        <div
            key={index}
            css={css`
                display: flex;
                flex-direction: column;

                @media only print {
                    margin-top: 40px;
                    page-break-after: always;
                }
            `}
        >
            {printPage(page)}
        </div>
    ));

    return (
        <div
            css={css`
                page-break-after: always;
            `}
        >
            <h1 className="section-title">{t('Doświadczenie')}</h1>
            <hr />
            {data1}
        </div>
    );
};

export { Experience };
