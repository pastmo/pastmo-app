/** @jsx jsx */
import { css, jsx } from '@emotion/core';

const PagePrint = ({ children, newLineAfter = true }: { children: any; newLineAfter?: boolean }) => {
    return (
        <section
            css={css`
                display: flex;
                position: relative;
                page-break-after: ${newLineAfter ? 'always' : 'avoid'};

                hr {
                    margin-bottom: 50px;
                }

                & > * {
                    width: 100%;
                    padding: 0px 50px;
                    margin: 0 auto;
                    min-height: ${newLineAfter ? 'calc(100vh - 70px)' : 'none'};
                }

                h1{
                    margin-top: 0;
                }
            `}
        >
            {children}
        </section>
    );
};
export { PagePrint };
