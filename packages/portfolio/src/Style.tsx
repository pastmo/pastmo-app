export interface Theme {
    name: string;
    backgroundColor: string;
    backgroundColor2: string;
    changeButton: string;
    textColor: string;
    activeColor: string;
}

const ColorsVariable = {
    white: '#fff',
    navyBlue: '#282c34',
    darkerBlue: '#636ce4',
    lightBlue: '#bbbff3',
    yellow: '#fbff00',
    blue: '#61dafb',
    darkGreen: '#384245'
};

const COLORS = {
    light: {
        name: 'light',
        backgroundColor: ColorsVariable.white,
        backgroundColor2: ColorsVariable.darkerBlue,
        changeButton: ColorsVariable.navyBlue,
        textColor: ColorsVariable.navyBlue,
        activeColor: ColorsVariable.yellow
    } as Theme,
    dark: {
        name: 'dark',
        backgroundColor: ColorsVariable.navyBlue,
        backgroundColor2: ColorsVariable.darkGreen,
        changeButton: ColorsVariable.white,
        textColor: ColorsVariable.white,
        activeColor: ColorsVariable.blue
    } as Theme
};

export { COLORS };
