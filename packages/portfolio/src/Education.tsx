/** @jsx jsx */
import { css, jsx } from '@emotion/core';
import { useTranslation } from 'react-i18next';
import pwr from './assets/education/pwr.png';

interface EduType {
    date: string;
    faculcy: string;
    company: string;
    logo?: string;
    degree: string;
    description: string;
    degreCourse: string;
}

const printPage = (pageData: EduType[]) =>
    pageData.map((exp) => (
        <div
            css={css`
                display: flex;
                @media print {
                    page-break-after: avoid;
                }
            `}
            key={exp.faculcy}
        >
            <div
                css={css`
                    flex: 0.2;
                    padding: 25px 0;
                `}
            >
                {exp.date}
            </div>
            <div
                css={css`
                    flex: 0.8;
                    display: flex;
                    border-left: 2px solid black;
                    padding: 25px;

                    @media not print {
                        flex-wrap: wrap;
                    }
                `}
            >
                <div
                    css={css`
                        width: 100px;
                        height: 104px;
                        background: url(${exp.logo ? exp.logo : ''}) no-repeat center;
                        background-size: 100% auto;
                        margin-right: 20px;
                        @media print {
                            flex: 0.3;
                        }
                    `}
                ></div>
                <div
                    css={css`
                        display: flex;
                        flex-direction: column;

                        @media print {
                            flex: 1;
                        }

                        p {
                            margin-bottom: 0;
                        }
                    `}
                >
                    <strong>{exp.company}</strong>
                    <span
                        css={css`
                            max-width: 600px;
                            font-style: italic;
                        `}
                    >
                        {exp.degree}
                    </span>
                    <p
                        css={css`
                            max-width: 600px;
                        `}
                    >
                        {exp.description}
                    </p>
                    <p
                        css={css`
                            max-width: 600px;
                        `}
                    >
                        {exp.degreCourse}
                    </p>
                </div>
            </div>
        </div>
    ));

const Education = () => {
    const { t, i18n } = useTranslation();

    const expData: EduType[] = [
        {
            date: '2012 - 2013',
            company: t('Politechnika Wrocławska'),
            logo: pwr,
            faculcy: 'weka',
            degree: t('Studia magisterskie'),
            description: t('Wydział Elektroniki'),
            degreCourse: t('Kierunek informatyka'),
        },
        {
            date: '2007 - 2012',
            company: t('Politechnika Wrocławska'),
            logo: pwr,
            faculcy: 'wemif',
            degree: t('Studia inżynierskie'),
            description: t('Wydział Elektroniki Mikrosystemów i Fotoniki'),
            degreCourse: t('Kierunek Elektronika i Telekomunikacja'),
        },
    ];

    return (
        <div
            css={css`
                page-break-after: always;
            `}
        >
            <h1 className="section-title">{t('Edukacja')}</h1>
            <hr />
            <div
                css={css`
                    display: flex;
                    flex-direction: column;

                    @media only print {
                        margin-top: 40px;
                    }
                `}
            >
                {printPage(expData)}
            </div>
        </div>
    );
};

export { Education };
