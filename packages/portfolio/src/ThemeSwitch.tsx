/** @jsx jsx */
import { css, jsx } from '@emotion/core';
import { useContext } from 'react';
import { FaMoon, FaSun } from 'react-icons/fa';
import { ThemeContext } from './App';
import { COLORS, Theme } from './Style';

const ThemeSwitch = ({ onThemeChange }: { onThemeChange: (theme: Theme) => void }) => {
    const theme = useContext(ThemeContext);

    const setLight = () => {
        onThemeChange(COLORS.light);
    };
    const setDark = () => {
        onThemeChange(COLORS.dark);
    };

    return (
        <div
            css={css`
                cursor: pointer;
                font-size: 24px;
                padding: 0px 20px;
                position: absolute;
                right: 0;
            `}
        >
            <FaSun
                onClick={setLight}
                css={css`
                    color: ${theme == COLORS.light ? theme.activeColor : theme.textColor};
                `}
            />
            <FaMoon
                onClick={setDark}
                css={css`
                    color: ${theme == COLORS.dark ? theme.activeColor : theme.textColor};
                `}
            />
        </div>
    );
};

export { ThemeSwitch };
