import React from 'react';
import { Contact } from './Contact';
import { DisplayMode } from './DisplayMode';
import { Education } from './Education';
import { Experience } from './Experience';
import { Header } from './Header';
import { PagePrint } from './PagePrint';
import { Projects } from './Projects';
import { Technology } from './Technology';

const PrintComponent = () => {
    return (
        <div>
            <PagePrint>
                <Header />
            </PagePrint>

            <PagePrint>
                <Technology  isPrint={true} />
            </PagePrint>
            <PagePrint newLineAfter={false}>
                <Education />
            </PagePrint>
            <PagePrint newLineAfter={false}>
                <Experience />
            </PagePrint>
            <PagePrint newLineAfter={true}>
                <Projects mode={DisplayMode.Print} />
            </PagePrint>
            <PagePrint newLineAfter={false}>
                <Contact mode={DisplayMode.Print} />
            </PagePrint>
        </div>
    );
};

export { PrintComponent };

