/** @jsx jsx */
import { css, jsx } from '@emotion/core';
import { useTranslation } from 'react-i18next';
import { useSearchParams } from 'react-router-dom';
import { DisplayMode } from './DisplayMode';
import linkedinIcon from './assets/contact/Linkedin_icon.svg';

const Contact = ({ mode = DisplayMode.Normal }: { mode?: DisplayMode }) => {
    const isPrinting = mode === DisplayMode.Print;

    const linkedin = 'https://www.linkedin.com/in/pawe%C5%82-rymsza-a8aa3844/';
    const email = 'pr@pastmo.pl';
    const [searchParams, setSearchParams] = useSearchParams();
    const { t, i18n } = useTranslation();

    const phone = searchParams.get('phone');

    return (
        <div
            css={css`
                @media not print {
                    min-height: calc(100vh - 210px);
                }
            `}
        >
            <h1 className="section-title">{t('Kontakt')}</h1>
            <hr />
            {!isPrinting ? (
                <div
                    css={css`
                        display: flex;
                        justify-content: space-around;
                        align-items: center;

                        @media print {
                            flex-direction: column;
                            align-items: start;
                        }
                    `}
                >
                    <div
                        css={css`
                            margin-bottom: 50px;
                        `}
                    >
                        <a href={linkedin} target="_blank">
                            <img
                                src={linkedinIcon}
                                css={css`
                                    @media print {
                                        max-width: 70px;
                                    }
                                `}
                            />
                        </a>
                    </div>
                    <div>
                        <div>
                            {' '}
                            Email: <a href={'mailto:' + email}>{email}</a>
                        </div>
                        {phone && (
                            <div>
                                {' '}
                                {t('Tel.')}: +48 {phone}
                            </div>
                        )}
                    </div>
                </div>
            ) : (
                <div
                    css={css`
                        display: flex;
                        flex-direction: column;
                        margin-bottom: 30px;

                        .project-row {
                            display: flex;

                            .label {
                                flex: 0.3;
                            }

                            .value {
                                flex: 0.7;
                            }
                        }
                    `}
                >
                    {phone && (
                        <div className="project-row">
                            <div className="label">{t('Tel.')}:</div>
                            <div className="value"> +48 {phone}</div>
                        </div>
                    )}
                    <div className="project-row">
                        <div className="label">Email:</div>
                        <div className="value">
                            <a href={'mailto:' + email}>{email}</a>
                        </div>
                    </div>
                    <div className="project-row">
                        <div className="label">Linkedin:</div>
                        <div className="value">
                            <a href={linkedin} target="_blank">
                                {linkedin}
                            </a>
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
};
export { Contact };
