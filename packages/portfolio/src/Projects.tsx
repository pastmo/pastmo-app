/**@jsx jsx */

import { css, jsx } from '@emotion/core';
import { useTranslation } from 'react-i18next';
import ImageGallery from 'react-image-gallery';
import creminiFaktury from './assets/projects/cremini-faktury.png';
import creminiKalendarz from './assets/projects/cremini-kalendarz.png';
import creminiKlienci from './assets/projects/cremini-klienci1.png';
import creminiPoczta from './assets/projects/cremini-poczta2.png';
import creminiPulpit from './assets/projects/cremini-pulpit.png';
import creminiZlecenia from './assets/projects/cremini-zleceniab.png';
import podejmijdecyzje3 from './assets/projects/podejmijdecyzje-opis.png';
import podejmijdecyzje2 from './assets/projects/podejmijdecyzje-z wynikiem.png';
import podejmijdecyzje from './assets/projects/podejmijdecyzje.png';
import robocker1 from './assets/projects/robocker1.png';
import robocker2 from './assets/projects/robocker2.png';
import portfolio from './assets/projects/portfolio.png';
import { DisplayMode } from './DisplayMode';

interface ProjectType {
    imgs: string[];
    name: string;
    descritpion: string;
    developmentDates: string;
    technology: string;
    repoLink?: string;
    demoLinks: string[];
    login?: string;
    password?: string;
}

const Projects = ({ mode = DisplayMode.Normal }: { mode?: DisplayMode }) => {
    const isPrinting = mode === DisplayMode.Print;
    const { t, i18n } = useTranslation();

    const projectsData: ProjectType[] = [
        {
            name: 'Robocker',
            descritpion: t('Gra mająca na celu ułatwienie nauki programowania.'),
            developmentDates: '2021-',
            technology: 'WebGl, BabylonJS, Java, Spring Boot',
            imgs: [robocker1, robocker2],
            repoLink: 'https://github.com/robocker/game',
            demoLinks: ['https://www.youtube.com/@RobockerEN', 'https://www.youtube.com/@RobockerPL'],
        },
        {
            name: 'Portfolio',
            descritpion: t('CV w formie strony internetowej.'),
            developmentDates: '2020-',
            technology: 'React',
            imgs: [portfolio],
            repoLink: 'https://bitbucket.org/pastmo/pastmo-app/src/master/packages/portfolio/',
            demoLinks: ['http://pr.pastmo.pl/'],
        },
        {
            name: t('Podejmij Decyzje'),
            descritpion: t('Jeden z modułów Cremini, funkcjonujący również jako osobna aplikacja.'),
            developmentDates: '2018',
            technology: 'PHP, Zend3, JQuery',
            imgs: [podejmijdecyzje, podejmijdecyzje2, podejmijdecyzje3],
            repoLink: 'https://bitbucket.org/pastmo/podejmij-decyzje',
            demoLinks: ['http://podejmijdecyzje.pastmo.pl/'],
            login: 'test@pastmo.pl',
            password: 'testoweKonto654!',
        },
        {
            name: 'Cremini',
            descritpion: t('System CRM'),
            developmentDates: '2017',
            technology: 'PHP, Zend3, JQuery',
            imgs: [creminiPulpit, creminiPoczta, creminiKalendarz, creminiZlecenia, creminiKlienci, creminiFaktury],
            repoLink: 'https://bitbucket.org/pastmo/cremini',
            demoLinks: ['http://cremini.pastmo.pl/'],
            login: 'test@pastmo.pl',
            password: 'testoweKonto76!',
        },
    ];

    const projects = projectsData.map((data) => {
        const images = data.imgs.map((img) => ({
            original: img,
            thumbnail: img,
        }));

        const links = data.demoLinks.map((link: string) => {
            return (
                <a href={link} target="_blank" key={link}>
                    {' '}
                    {link}
                </a>
            );
        });

        return (
            <div key={data.name}>
                {!isPrinting ? (
                    <div

                        css={css`
                            display: flex;
                            flex-direction: column;

                            margin-top: 70px;
                            padding: 10px;

                            -moz-box-shadow: 0px 0px 6px -1px black;
                            -webkit-box-shadow: 0px 0px 6px -1px black;
                            box-shadow: 0px 0px 6px -1px black;
                        `}
                    >
                        <span
                            css={css`
                                text-align: center;
                                padding: 0px 15px;
                                text-transform: uppercase;
                                font-size: 23px;
                            `}
                        >
                            {data.name}
                        </span>
                        <div
                            css={css`
                                @media (min-width: 1200px) {
                                    .image-gallery-left-nav {
                                        left: -80px;
                                    }

                                    .image-gallery-right-nav {
                                        right: -80px;
                                    }
                                }
                            `}
                        >
                            <div
                                css={css`
                                    width: 80%;
                                    margin: 0 auto;

                                    @media (max-width: 400px) {
                                        width: 100%;
                                    }
                                `}
                            >
                                <ImageGallery items={images} showThumbnails={false} />{' '}
                            </div>
                        </div>{' '}
                        <div
                            css={css`
                                display: flex;
                                flex-direction: column;
                                margin-bottom: 30px;

                                .project-row {
                                    display: flex;

                                    .label {
                                        flex: 0.3;
                                    }

                                    .value {
                                        flex: 0.7;
                                    }
                                }
                            `}
                        >
                            {' '}
                            <div className="project-row">
                                <div className="label">{t('Opis')}:</div>
                                <div className="value">{data.descritpion}</div>
                            </div>
                            <div className="project-row">
                                <div className="label">{t('Lata aktywności')}:</div>
                                <div className="value">{data.developmentDates}</div>
                            </div>
                            <div className="project-row">
                                <div className="label">{t('Repozytorium')}:</div>
                                <div className="value">
                                    <a href={data.repoLink} target="_blank">
                                        {data.repoLink}
                                    </a>
                                </div>
                            </div>
                            <div className="project-row">
                                <div className="label">URL:</div>
                                <div className="value">{links}</div>
                            </div>
                            {data.login && (
                                <div className="project-row">
                                    <div className="label">Login:</div>
                                    <div className="value">{data.login}</div>
                                </div>
                            )}
                            {data.password && (
                                <div className="project-row">
                                    <div className="label">{t('Hasło')}:</div>
                                    <div className="value">{data.password}</div>
                                </div>
                            )}
                        </div>{' '}
                    </div>
                ) : (
                    <div

                        css={css`
                            display: flex;
                            flex-direction: column;
                            margin-bottom: 30px;

                            .project-row {
                                display: flex;

                                .label {
                                    flex: 0.3;
                                }

                                .value {
                                    flex: 0.7;
                                }
                            }
                        `}
                    >
                        <h4>{data.name}</h4>
                        <div className="project-row">
                            <div className="label">{t('Opis')}:</div>
                            <div className="value">{data.descritpion}</div>
                        </div>
                        <div className="project-row">
                            <div className="label">{t('Lata aktywności')}:</div>
                            <div className="value">{data.developmentDates}</div>
                        </div>
                        <div className="project-row">
                            <div className="label">{t('Repozytorium')}:</div>
                            <div className="value">
                                <a href={data.repoLink} target="_blank">
                                    {data.repoLink}
                                </a>
                            </div>
                        </div>
                        <div className="project-row">
                            <div className="label">URL:</div>
                            <div className="value">{links}</div>
                        </div>
                        {data.login && (
                            <div className="project-row">
                                <div className="label">Login:</div>
                                <div className="value">{data.login}</div>
                            </div>
                        )}
                        {data.password && (
                            <div className="project-row">
                                <div className="label">{t('Hasło')}:</div>
                                <div className="value">{data.password}</div>
                            </div>
                        )}
                    </div>
                )}
            </div>
        );
    });

    return (
        <div>
            {' '}
            <h1 className="section-title">{t('Przykładowe Projekty')}</h1>
            <hr />
            <div> {projects}</div>
        </div>
    );
};

export { Projects };
