/** @jsx jsx */

import { css, jsx } from '@emotion/core';
import { RefObject, useContext, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { FaAngleDown, FaAngleUp } from 'react-icons/fa';
import { FiPrinter } from 'react-icons/fi';
import ReactToPrint from 'react-to-print';
import { ThemeContext } from './App';
import { DEFAULT_LANG } from './i18n';
import { PrintComponent } from './PrintComponent';
import { Theme } from './Style';

const Menu = ({ setTheme }: { setTheme: (theme: Theme) => void }) => {
    const theme = useContext(ThemeContext);
    const [expanded, setExpanded] = useState(true);
    const [isOnTop, setIsOnTop] = useState(window.scrollY === 0);

    const [lang, setLang] = useState(DEFAULT_LANG);

    const { t, i18n } = useTranslation();

    const header = useRef(null);
    const education = useRef(null);
    const projects = useRef(null);
    const technology = useRef(null);
    const experience = useRef(null);
    const contact = useRef(null);
    const printRef = useRef(null);

    const smartMenu = 832;

    const buttonStyle = `
        text-align: center;
        padding: 6px 16px;
        text-align: center;
        padding: 18px 16px;
        text-decoration: none;
        font-size: 20px;
        cursor:pointer;

        &:hover {
            color: ${theme.backgroundColor};
            background: ${theme.textColor};
        }
    `;

    useEffect(() => {
        window.addEventListener('scroll', handleScroll);

        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    });

    const handleScroll = () => {
        setIsOnTop(window.scrollY === 0);
    };

    const scrollToRef = (ref: RefObject<HTMLLinkElement>) =>
        window.scrollTo(0, ref.current ? ref.current.offsetTop : 0);

    const boxShadow = ` -moz-box-shadow: 0px 0px 20px -8px black;
        -webkit-box-shadow: 0px 0px 20px -8px black;
        box-shadow: 0px 0px 20px -8px black;`;

    return (
        <div
            css={css`
            @media print{
                display:none
            }

                position: fixed;
                z-index: 10;
                width: 100vw;
                display: flex;
                justify-content: center;
                background: ${theme.backgroundColor};
                height: 60px;
                border-bottom: ${isOnTop ? 'none' : '1px solid rgba(0, 0, 0, 0.2)'};

                ${!isOnTop ? boxShadow : ''}
            }

                @media (max-width: ${smartMenu}px) {
                    height: auto;
                }
            `}
        >
            {expanded && (
                <nav
                    css={css`
                        display: flex;

                        @media (max-width: ${smartMenu}px) {
                            flex-direction: column;
                        }

                        > a {
                            color: ${theme.textColor};
                            text-transform: uppercase;

                            ${buttonStyle}
                        }
                    `}
                >
                    <a ref={header} href="#header" onClick={() => scrollToRef(header)}>
                        {t('O mnie')}
                    </a>
                    <a ref={technology} href="#technology" onClick={() => scrollToRef(technology)}>
                        {t('Technologie')}
                    </a>
                    <a ref={education} href="#education" onClick={() => scrollToRef(education)}>
                        {t('Edukacja')}
                    </a>
                    <a ref={experience} href="#experience" onClick={() => scrollToRef(experience)}>
                        {t('Doświadczenie')}
                    </a>
                    <a ref={projects} href="#projects" onClick={() => scrollToRef(projects)}>
                        {t('Przykładowe Projekty')}
                    </a>
                    <a ref={contact} href="#contact" onClick={() => scrollToRef(contact)}>
                        {t('Kontakt')}
                    </a>
                    <ReactToPrint
                        trigger={() => (
                            <span
                                css={css`
                                    ${buttonStyle}
                                `}
                            >
                                {' '}
                                <FiPrinter />
                            </span>
                        )}
                        content={() => printRef.current}
                    />

                    <div
                        ref={printRef}
                        css={css`
                            @media not print {
                                display: none;
                            }
                        `}
                    >
                        <PrintComponent />
                    </div>

                    <span
                        css={css`
                            text-align: center;
                            display: flex;
                            justify-content: center;

                            span {
                                font-size: 20px;
                                cursor: pointer;
                                padding: 17px 16px;

                                &:hover {
                                    color: ${theme.backgroundColor};
                                    background: ${theme.textColor};
                                }
                                &.selected {
                                    color: ${theme.backgroundColor};
                                    background: rgba(40, 44, 52, 0.25);
                                    cursor: default;
                                }
                            }
                        `}
                    >
                        <span
                            className={lang == 'pl' ? 'selected' : ''}
                            onClick={() => {
                                i18n.changeLanguage('pl');
                                setLang('pl');
                            }}
                        >
                            PL
                        </span>
                        <span
                            className={lang == 'en' ? 'selected' : ''}
                            css={css``}
                            onClick={() => {
                                i18n.changeLanguage('en');
                                setLang('en');
                            }}
                        >
                            EN
                        </span>
                    </span>

                    <span
                        css={[
                            css`
                                ${buttonStyle}
                            `,
                            css`
                                @media (min-width: ${smartMenu}px) {
                                    display: none;
                                }
                            `
                        ]}
                        onClick={() => {
                            setExpanded(false);
                        }}
                    >
                        <FaAngleUp />
                    </span>
                </nav>
            )}

            {!expanded && (
                <span
                    css={css`
                        ${buttonStyle}
                    `}
                    onClick={() => {
                        setExpanded(true);
                    }}
                >
                    <FaAngleDown />
                </span>
            )}
        </div>
    );
};

export { Menu };

