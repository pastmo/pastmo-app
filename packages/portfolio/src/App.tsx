/** @jsx jsx */
import { jsx } from '@emotion/core';
import 'animate.css/animate.min.css';
import { createContext, useState } from 'react';
import './App.css';
import { Contact } from './Contact';
import { Education } from './Education';
import { Experience } from './Experience';
import { Header } from './Header';
import { Menu } from './Menu';
import { Page } from './Page';
import { Projects } from './Projects';
import { COLORS } from './Style';
import { Technology } from './Technology';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

export const ThemeContext = createContext(COLORS.dark);

const App = () => {
    const [theme, setTheme] = useState(COLORS.light);

    return (
        <ThemeContext.Provider value={theme}>
            <BrowserRouter>
                <Routes>
                    <Route
                        path="/"
                        element={
                            <div className="App">
                                <Menu setTheme={setTheme} />
                                <Page selector="header" marginBottom="70px">
                                    <Header />
                                </Page>
                                <Page selector="technology">
                                    <Technology isPrint={false} />
                                </Page>
                                <Page selector="education">
                                    <Education />
                                </Page>
                                <Page selector="experience">
                                    <Experience />
                                </Page>
                                <Page selector="projects">
                                    <Projects />
                                </Page>
                                <Page selector="contact">
                                    <Contact />
                                </Page>
                            </div>
                        }
                    ></Route>
                </Routes>
            </BrowserRouter>
        </ThemeContext.Provider>
    );
};

export default App;
