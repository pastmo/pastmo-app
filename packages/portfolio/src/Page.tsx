/** @jsx jsx */
import { css, jsx } from '@emotion/core';
import ScrollAnimation from 'react-animate-on-scroll';
import { IoIosArrowDown } from 'react-icons/io';

const StandarSectionMaxWith = '1100px';
const Page = ({
    children,
    selector,
    marginBottom = '90px'
}: {
    children: any;
    selector: string;
    marginBottom?: string;
}) => {
    return (
        <ScrollAnimation animateIn="fadeIn">
            <section
                id={selector}
                css={css`
                    margin-bottom: ${marginBottom};
                    display: flex;
                    position: relative;

                    & > * {
                        width: 100%;
                        padding: 73px 20px 0 20px;
                        margin: 0 auto;
                        max-width: ${StandarSectionMaxWith};
                    }
                `}
            >
                {children}
                {false && (
                    <div
                        css={css`
                            position: absolute;
                            display: flex;
                            bottom: 0;
                            justify-content: center;
                        `}
                    >
                        <span>
                            <IoIosArrowDown />
                        </span>
                    </div>
                )}
            </section>
        </ScrollAnimation>
    );
};
export { Page, StandarSectionMaxWith };
