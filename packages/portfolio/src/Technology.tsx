/**@jsx jsx */
import { css, jsx } from '@emotion/core';
import { useContext } from 'react';
import { useTranslation } from 'react-i18next';
import StarRatings from 'react-star-ratings';
import { ThemeContext } from './App';
import { StandarSectionMaxWith } from './Page';
import gcp from './assets/technology/Google_Cloud.svg';
import phpIcon from './assets/technology/PHP-logo.svg';
import phpunit from './assets/technology/PHPUnit_Logo.svg';
import postgresql from './assets/technology/Postgresql_elephant.svg';
import react from './assets/technology/React.svg';
import angular from './assets/technology/angular.svg';
import angularMaterial from './assets/technology/angular-material-logo.svg';
import agGrid from './assets/technology/logo-bright-theme-black-writing.svg';
import cypress from './assets/technology/cypress.svg';
import docker from './assets/technology/docker-logo-svg-vector.svg';
import git from './assets/technology/git.svg';
import jasmine from './assets/technology/jasmine.svg';
import java from './assets/technology/java.svg';
import jenkins from './assets/technology/jenkins.svg';
import jquery from './assets/technology/jquery-icon.svg';
import jsTs from './assets/technology/js-ts.svg';
import karma from './assets/technology/karma.svg';
import mysql from './assets/technology/mysql.svg';
import nx from './assets/technology/nx.svg';
import python from './assets/technology/python-logo-only.svg';
import node from './assets/technology/nodejs.svg';
import redis from './assets/technology/redis.svg';
import redux from './assets/technology/redux.svg';
import spring from './assets/technology/spring-icon.svg';
import symfony from './assets/technology/symfony_black_03.svg';
import jest from './assets/technology/jest-logo-svg-vector.svg';
import zend from './assets/technology/zf-logo-mark.png';

interface TechnologyItem {
    name: string;
    link?: string;
    img?: string;
    showChild?: boolean;
    hideText?: boolean;
    rating?: number;
    children?: TechnologyItem[];
}

const Technology = ({ isPrint = false }: { isPrint: boolean }) => {
    const theme = useContext(ThemeContext);
    const { t } = useTranslation();

    const technologies: TechnologyItem[] = [
        {
            name: 'Frontend',
            children: [
                { name: 'Angular', img: angular, rating: 5 },
                { name: 'Angular Material', img: angularMaterial, rating: 5 },
                { name: 'Ag Grid', img: agGrid, rating: 4, hideText: true },
                { name: 'Javascript/Typescript', img: jsTs, rating: 5 },
                { name: 'React', img: react, rating: 4 },
                { name: 'Redux', img: redux, rating: 3 },

                { name: 'JQuery', img: jquery, rating: 3 },
            ],
        },
        {
            name: 'Backend',
            children: [
                { name: 'PHP', img: phpIcon, rating: 4 },
                { name: 'Zend Framework', img: zend, rating: 4 },
                { name: 'Symfony', img: symfony, rating: 3 },
                { name: 'NodeJs', img: node, rating: 3, hideText: true },
                { name: 'Java', img: java, rating: 2 },
                { name: 'Spring', img: spring, rating: 1 },
                { name: 'Python', img: python, rating: 2 },
                { name: 'MySQL', img: mysql, rating: 4 },
                { name: 'PostgreSQL', img: postgresql, rating: 4 },
                { name: 'Redis', img: redis, rating: 2 },
            ],
        },
        {
            name: t('Narzędzia'),
            children: [
                { name: 'Git', img: git, rating: 4 },
                { name: 'Docker', img: docker, rating: 4 },
                { name: 'NX', img: nx, rating: 3 },
                { name: 'GCP', img: gcp, rating: 2 },
                { name: 'Jenkins', img: jenkins, rating: 2 },
            ],
        },
        {
            name: t('Testy'),
            children: [
                { name: 'Jest', img: jest, rating: 4 },
                { name: 'Karma', img: karma, rating: 4 },
                { name: 'Jasmine', img: jasmine, rating: 4 },
                { name: 'Cypress', img: cypress, rating: 3 },
                { name: 'PHPUnit', img: phpunit, hideText: true, rating: 4 },
            ],
        },
    ];

    const printTechnology = (item: TechnologyItem, key: number) => {
        const children = item.children ? item.children.map(secondLevel) : [];

        const containerStyle = css``;
        const itemKey = item.name + key;

        return (
            <div
                key={itemKey}
                css={css`
                    padding: 10px;
                    margin: 10px;
                    -moz-box-shadow: 0px 0px 6px -1px black;
                    -webkit-box-shadow: 0px 0px 6px -1px black;
                    box-shadow: 0px 0px 6px -1px black;

                    @media print {
                        -moz-box-shadow: none;
                        -webkit-box-shadow: none;
                        box-shadow: none;
                    }

                    page-break-inside: avoid;

                    @media (max-width: 700px) {
                        padding: 20px;
                    }
                `}
            >
                <span
                    css={[
                        containerStyle,
                        css`
                            padding: 0px 15px;
                            text-transform: uppercase;
                            font-size: 23px;
                        `,
                    ]}
                >
                    {item.name}
                </span>

                <div
                    css={css`
                        display: flex;
                        flex-wrap: wrap;
                        margin-top: 45px;
                        height: 77%;
                        column-gap: 24px;

                        justify-content: space-around;
                    `}
                >
                    {children}
                </div>
            </div>
        );
    };

    const secondLevel = (item: TechnologyItem, key: number) => {
        const itemKey = item.name + key;

        return (
            <div
                key={itemKey}
                css={css`
                    display: flex;
                    flex-direction: column;
                    page-break-inside: avoid;
                    align-items: center;
                `}
            >
                <img
                    css={css`
                        margin: 11px;
                        height: 75px;
                        max-width: 170px;
                    `}
                    src={item.img}
                    title={item.name}
                />
                <StarRatings
                    rating={item.rating}
                    starRatedColor={isPrint ? 'black' : '#ffd700'}
                    numberOfStars={5}
                    name="rating"
                    starDimension="12px"
                />
                {!item.hideText && <span>{item.name}</span>}
            </div>
        );
    };

    const techItems = technologies.map(printTechnology);

    return (
        <div
            css={css`
                background-color: ${theme.backgroundColor};
                color: ${theme.textColor};
                max-width: 1317px;
            `}
        >
            <div></div>
            <h1
                className="section-title"
                css={css`
                    max-width: ${StandarSectionMaxWith};
                    margin-left: auto;
                    margin-right: auto;
                `}
            >
                {t('Technologie')}
            </h1>
            <hr
                css={css`
                    max-width: ${StandarSectionMaxWith};
                    margin-left: auto;
                    margin-right: auto;
                `}
            />
            <div
                css={css`
                    display: flex;
                    justify-content: space-around;
                    align-items: stretch !important;
                    text-align: center;
                    flex-direction: column;
                `}
            >
                {techItems}
            </div>
        </div>
    );
};

export { Technology };
