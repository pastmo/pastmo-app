/** @jsx jsx */
import { css, jsx } from '@emotion/core';
import { useContext } from 'react';
import { ThemeContext } from './App';
import logoBlue from './assets/logo-blue.png';
import logoRed from './assets/logo-red.png';

const Header = () => {
    const theme = useContext(ThemeContext);

    return (
        <header
            className="App-header"
            css={css`
                min-height: 100vh;
                display: flex;
                align-items: center;
                justify-content: space-around;
                flex-wrap: wrap;
                font-size: calc(10px + 2vmin);
                background-color: ${theme.BACKGROUND_COLOR};
            `}
        >
            <img
                src={theme.NAME === 'light' ? logoBlue : logoRed}
                css={css`
                    height: 40vmin;
                `}
            />
            <div
                css={css`
                    color: ${theme.TEXT_COLOR};
                `}
            >
                {' '}
                Profesjonalne usługi IT
            </div>
            <div
                css={css`
                    position: absolute;
                    bottom: 5px;
                `}
            >
                <a
                    css={css`
                        color: ${theme.TEXT_COLOR};
                        text-decoration: none;

                        &:hover {
                            color: ${theme.ACTIVE_COLOR};
                        }
                    `}
                    href="http://pr.pastmo.pl/"
                >
                    portfolio
                </a>
            </div>
        </header>
    );
};

export { Header };
