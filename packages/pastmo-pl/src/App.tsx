/** @jsx jsx */
import { jsx } from '@emotion/core';
import { createContext, useState } from 'react';
import './App.css';
import { Header } from './Header';
import { Page } from './Page';
import { COLORS } from './Style';
import { ThemeSwitch } from './ThemeSwitch';

export const ThemeContext = createContext(COLORS.dark);

const App = () => {
    const [theme, setTheme] = useState(COLORS.light);

    return (
        <ThemeContext.Provider value={theme}>
            <div className="App">
                <Page selector="header">
                    <ThemeSwitch onThemeChange={setTheme} />
                    <Header />
                </Page>
            </div>
        </ThemeContext.Provider>
    );
};

export default App;
