/** @jsx jsx */
import { jsx } from '@emotion/core';

const Page = ({ children, selector }: { children: any; selector: string }) => {
    return <section id={selector}>{children}</section>;
};
export { Page };
