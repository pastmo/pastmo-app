export interface Theme {
    NAME: string;
    BACKGROUND_COLOR: string;
    CHANGE_BUTTON: string;
    TEXT_COLOR: string;
    ACTIVE_COLOR: string;
}

const COLORS = {
    light: {
        NAME: 'light',
        BACKGROUND_COLOR: ' #fff',
        CHANGE_BUTTON: '#282c34',
        TEXT_COLOR: '#282c34',
        ACTIVE_COLOR: '#e4ff00'
    } as Theme,
    dark: {
        NAME: 'dark',
        BACKGROUND_COLOR: '#282c34',
        CHANGE_BUTTON: '#fff',
        TEXT_COLOR: '#fff',
        ACTIVE_COLOR: '#61dafb'
    } as Theme
};

export { COLORS };
