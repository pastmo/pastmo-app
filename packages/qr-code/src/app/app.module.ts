import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { QrModule } from '../qr/qr.module';
import { AppComponent } from './app.component';
import { DisplayComponent } from './display/display.component';
import { ReadComponent } from './read/read.component';

@NgModule({
    declarations: [AppComponent, ReadComponent, DisplayComponent],
    imports: [
        BrowserModule,
        FormsModule,
        QrModule,
        RouterModule.forRoot([
            { path: 'read', component: ReadComponent },
            { path: 'display', component: DisplayComponent }
        ])
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}
