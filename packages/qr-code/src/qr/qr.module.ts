import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { NgxKjuaModule } from 'ngx-kjua';
import { QrDisplayComponent } from './qr-display/qr-display.component';
import { QrReadComponent } from './qr-read/qr-read.component';

@NgModule({
    declarations: [QrReadComponent, QrDisplayComponent],
    imports: [CommonModule, ZXingScannerModule, NgxKjuaModule],
    exports: [QrReadComponent, QrDisplayComponent]
})
export class QrModule {}
