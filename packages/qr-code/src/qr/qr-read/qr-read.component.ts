import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-qr-read',
    templateUrl: './qr-read.component.html',
    styleUrls: ['./qr-read.component.css']
})
export class QrReadComponent implements OnInit {
    constructor() {}

    @Output()
    scanSuccess: EventEmitter<string> = new EventEmitter<string>();

    ngOnInit(): void {}

    onScanSuccess(data) {
        this.scanSuccess.emit(data);
    }
}
